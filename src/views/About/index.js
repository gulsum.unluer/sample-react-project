import React, { useState, useRef } from 'react'
import BPInput from '../../components/BPInput'
import BPModal from '../../components/BPModal'
import Button from '../../components/Button'
import { Intent } from '@blueprintjs/core'

export default function About() {
  const [inputValue, setInputValue] = useState('')
  const [showModal, setShowModal] = useState(false)
  const refinput = useRef()

  const onChangeHandler = (val) => {
    setInputValue(val)
  }

  const onCloseHandler = () => {
    setShowModal(false)
  }
  const onShowModal = () => {
    setShowModal(true)
  }

  return (
    <div>
      <Button
        type='button'
        text='Open the modal'
        onClick={onShowModal}
        intent={Intent.DANGER}
      />
      <BPModal show={showModal} title='Modal' onClose={onCloseHandler}>
        <BPInput
          type='text'
          className='bp3-icon'
          value={inputValue}
          inputRef={refinput}
          placeholder='Enter an input'
          onChange={onChangeHandler}
        />
        <p>{inputValue}</p>
      </BPModal>
    </div>
  )
}
