import React, { useState, useEffect, useRef } from 'react'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCoffee } from '@fortawesome/free-solid-svg-icons'

import Button from '../../components/Button'
import Input from '../../components/Input'
import Modal from '../../components/Modal'
import BPSelect from '../../components/BPSelect'

function Home () {
  const [state, setState] = useState('')
  const [value, setValue] = useState('')
  const [selectedColor, setSelectedColor] = useState()
  // const [showModal, setShowModal] = useState(false);
  const refModal = useRef()

  const items = [
    { id: 1, text: 'Blue' },
    { id: 2, text: 'Red' },
    { id: 3, text: 'Green' },
    { id: 4, text: 'Pink' }
  ]
  const handleOnSelectedItem = (item) => {
    setSelectedColor(item)
  }

  const handleOnChange = (event) => {
    console.log(event)
    setState(event.target && event.target.value)
  }
  const handleOnClick = () => {
    setValue(state)
    refModal.current.show()
    // setShowModal(true);
  }

  useEffect(() => {
    setState('Merhaba')
  }, [value])

  return (
    <>

      <BPSelect items={items} placeholder='Please choose a color' onItemSelect={handleOnSelectedItem} value={selectedColor} />
      <div style={{ display: 'flex-grid', padding: '10px', margin: '20px' }}>
        <Input value={state} onChange={handleOnChange} />
        <Button className='button-danger' onClick={handleOnClick}>
          <FontAwesomeIcon
            icon={faCoffee}
            style={{ color: 'white', paddingRight: 10 }}
          />
            Değeri Göster
        </Button>
        <div>{state}</div>
        <div>{value}</div>

        <Modal ref={refModal} title='Deneme Modal' onClose={() => { refModal.current.hide() }} onAccept={() => { refModal.current.hide() }}>
          <h1>Title</h1>
          <h5>Subtitle</h5>
          <p> Bu bir modal</p>
        </Modal>
      </div>
    </>
  )
}

export default Home
