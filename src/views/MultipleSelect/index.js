import React, { useState } from 'react'

import SelectDialog from './SelectDialog'
import SelectCard from './SelectCard'
const MultipleSelect = () => {
  const [isOpen, setOpen] = useState(false)
  const [msData, setMsData] = useState([])
  const dialogClose = (data) => {
    setOpen(false)
    // console.log(data)
    if (data.accept) {
      setMsData(data.selectedFilms)
    }
  }
  return (
    <div style={{ padding: '20px' }}>
      <SelectDialog open={isOpen} closed={dialogClose} />

      <SelectCard films={msData} handleClick={(val) => setOpen(val)} />
    </div>
  )
}
export default MultipleSelect
