import React, { useState } from "react"
import {
  AnchorButton,
  Button,
  Classes,
  MenuItem,
  Code,
  Dialog,
  H5,
  Intent,
  Switch,
  Tooltip,
  ITagProps,
} from "@blueprintjs/core"
import { ItemRenderer, MultiSelect } from "@blueprintjs/select"
import {
  areFilmsEqual,
  arrayContainsFilm,
  createFilm,
  filmSelectProps,
  IFilm,
  maybeAddCreatedFilmToArrays,
  maybeDeleteCreatedFilmFromArrays,
  renderCreateFilmOption,
  TOP_100_FILMS,
} from "./films"

const INTENTS = [
  Intent.NONE,
  Intent.PRIMARY,
  Intent.SUCCESS,
  Intent.DANGER,
  Intent.WARNING,
]
const SelectDialog = ({ closed, open }) => {
  const [fstate, setfsState] = {
    allowCreate: false,
    createdItems: [],
    fill: false,
    films: [],
    hasInitialContent: false,
    intent: false,
    items: filmSelectProps.items,
    openOnKeyDown: false,
    popoverMinimal: true,
    resetOnSelect: true,
    tagMinimal: false,
  }
  const handleSwitchChange = (prop) => {
    return (event) => {
      const checked = event.currentTarget.checked
      setfsState((state) => ({ ...state, [prop]: checked }))
    }
  }
  const handleClear = () => setfsState({ films: [] })

  const handleFilmsPaste = (films) => {
    // On paste, don't bother with deselecting already selected values, just
    // add the new ones.
    selectFilms(films)
  }

  handleFilmSelect = (film) => {
    if (isFilmSelected(film)) {
      selectFilm(film)
    } else {
      deselectFilm(getSelectedFilmIndex(film))
    }
  }
  const deselectFilm = (index) => {
    const { films } = fstate

    const film = films[index]
    const { createdItems, items } = maybeDeleteCreatedFilmFromArrays(
      this.state.items,
      this.state.createdItems,
      film
    )

    // Delete the item if the user manually created it.
    setfsState({
      createdItems: nextCreatedItems,
      films: films.filter((_film, i) => i !== index),
      items: nextItems,
    })
  }
  const handleClose = (data) => {
    closed(data)
  }

  return (
    <Dialog
      icon='info-sign'
      onClose={() => handleClose(false)}
      title='Make You Selection'
      isOpen={open}
    >
      <div className={Classes.DIALOG_BODY} style={{ minHeight: "300px" }}>
        dasdasd
      </div>
      <div className={Classes.DIALOG_FOOTER}>
        <div className={Classes.DIALOG_FOOTER_ACTIONS}>
          <Tooltip content='Cancel'>
            <Button
              intent={Intent.DANGER}
              type='button'
              onClick={() => handleClose(false)}
            >
              Close
            </Button>
          </Tooltip>
          <Tooltip content='Save your Selection'>
            <Button
              intent={Intent.PRIMARY}
              type='button'
              onClick={() => handleClose(true)}
            >
              Save
            </Button>
          </Tooltip>
        </div>
      </div>
    </Dialog>
  )
}
export default SelectDialog
