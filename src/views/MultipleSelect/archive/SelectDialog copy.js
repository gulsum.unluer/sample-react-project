import React, { useState } from "react"
import {
  AnchorButton,
  Button,
  Classes,
  MenuItem,
  Code,
  Dialog,
  H5,
  Intent,
  Switch,
  Tooltip,
} from "@blueprintjs/core"
import { ItemRenderer, MultiSelect } from "@blueprintjs/select"

import {
  areFilmsEqual,
  arrayContainsFilm,
  createFilm,
  maybeAddCreatedFilmToArrays,
  maybeDeleteCreatedFilmFromArrays,
  renderCreateFilmOption,
  TOP_100_FILMS,
} from "./films"
const INTENTS = [
  Intent.NONE,
  Intent.PRIMARY,
  Intent.SUCCESS,
  Intent.DANGER,
  Intent.WARNING,
]
const SelectDialog = ({ closed, open }) => {
  const [filmsState, setFilms] = useState({})
  const {
    allowCreate,
    films,
    hasInitialContent,
    tagMinimal,
    popoverMinimal,
    ...flags
  } = filmsState

  const handleSwitchChange = (prop) => {
    return (event) => {
      const checked = event.currentTarget.checked
      setFilms((state) => ({ ...state, [prop]: checked }))
    }
  }
  const handleAllowCreateChange = handleSwitchChange("allowCreate")
  const handleKeyDownChange = handleSwitchChange("openOnKeyDown")
  const handleResetChange = handleSwitchChange("resetOnSelect")
  const handlePopoverMinimalChange = handleSwitchChange("popoverMinimal")
  const handleTagMinimalChange = handleSwitchChange("tagMinimal")
  const handleFillChange = handleSwitchChange("fill")
  const handleIntentChange = handleSwitchChange("intent")
  const handleInitialContentChange = handleSwitchChange("hasInitialContent")

  const getTagProps = (_value, index) => ({
    intent: filmsState.intent ? INTENTS[index % INTENTS.length] : Intent.NONE,
    minimal: tagMinimal,
  })
  const handleClose = (data) => {
    closed(data)
  }
  const initialContent = filmsState.hasInitialContent ? (
    <MenuItem disabled={true} text={`${TOP_100_FILMS.length} items loaded.`} />
  ) : // explicit undefined (not null) for default behavior (show full list)
  undefined
  const maybeCreateNewItemFromQuery = allowCreate ? createFilm : undefined
  const maybeCreateNewItemRenderer = allowCreate ? renderCreateFilmOption : null

  const clearButton =
    films?.length > 0 ? (
      <Button icon='cross' minimal={true} onClick={handleClear} />
    ) : undefined
  const handleFilmSelect = (film) => {
    if (!isFilmSelected(film)) {
      selectFilm(film)
    } else {
      deselectFilm(getSelectedFilmIndex(film))
    }
  }

  const handleFilmsPaste = (films) => {
    // On paste, don't bother with deselecting already selected values, just
    // add the new ones.
    selectFilms(films)
  }

  const handleClear = () => setFilms({ films: [] })
  const deselectFilm = (index) => {
    const { films } = filmsState

    const film = films[index]
    const {
      createdItems: nextCreatedItems,
      items: nextItems,
    } = maybeDeleteCreatedFilmFromArrays(
      filmsState.items,
      filmsState.createdItems,
      film
    )

    // Delete the item if the user manually created it.
    setFilms({
      createdItems: nextCreatedItems,
      films: films.filter((_film, i) => i !== index),
      items: nextItems,
    })
  }
  const selectFilms = (filmsToSelect) => {
    const { createdItems, films, items } = filmsState

    let nextCreatedItems = createdItems.slice()
    let nextFilms = films.slice()
    let nextItems = items?.slice()

    filmsToSelect.forEach((film) => {
      const results = maybeAddCreatedFilmToArrays(
        nextItems,
        nextCreatedItems,
        film
      )
      nextItems = results.items
      nextCreatedItems = results.createdItems
      // Avoid re-creating an item that is already selected (the "Create
      // Item" option will be shown even if it matches an already selected
      // item).
      nextFilms = !arrayContainsFilm(nextFilms, film)
        ? [...nextFilms, film]
        : nextFilms
    })

    setFilms({
      createdItems: nextCreatedItems,
      films: nextFilms,
      items: nextItems,
    })
  }
  const selectFilm = (film) => {
    selectFilms([film])
  }
  // NOTE: not using Films.itemRenderer here so we can set icons.
  const renderFilm = (film, { modifiers, handleClick }) => {
    if (!modifiers.matchesPredicate) {
      return null
    }
    return (
      <MenuItem
        active={modifiers.active}
        icon={isFilmSelected(film) ? "tick" : "blank"}
        key={film.rank}
        label={film.year.toString()}
        onClick={handleClick}
        text={`${film.rank}. ${film.title}`}
        shouldDismissPopover={false}
      />
    )
  }

  const handleTagRemove = (_tag, index) => {
    deselectFilm(index)
  }

  const getSelectedFilmIndex = (film) => {
    return filmsState.films.indexOf(film)
  }

  const isFilmSelected = (film) => {
    return getSelectedFilmIndex(film) !== -1
  }
  const renderTag = (film) => film.title
  const renderOptions = () => {
    return (
      <>
        <H5>Props</H5>
        <Switch
          label='Open popover on key down'
          checked={filmsState.openOnKeyDown}
          onChange={handleKeyDownChange}
        />
        <Switch
          label='Reset query on select'
          checked={filmsState.resetOnSelect}
          onChange={handleResetChange}
        />
        <Switch
          label='Use initial content'
          checked={filmsState.hasInitialContent}
          onChange={handleInitialContentChange}
        />
        <Switch
          label='Allow creating new films'
          checked={filmsState.allowCreate}
          onChange={handleAllowCreateChange}
        />
        <Switch
          label='Fill container width'
          checked={filmsState.fill}
          onChange={handleFillChange}
        />
        <H5>Tag props</H5>
        <Switch
          label='Minimal tag style'
          checked={filmsState.tagMinimal}
          onChange={handleTagMinimalChange}
        />
        <Switch
          label='Cycle through tag intents'
          checked={filmsState.intent}
          onChange={handleIntentChange}
        />
        <H5>Popover props</H5>
        <Switch
          label='Minimal popover style'
          checked={filmsState.popoverMinimal}
          onChange={handlePopoverMinimalChange}
        />
      </>
    )
  }
  return (
    <Dialog
      icon='info-sign'
      onClose={() => handleClose(false)}
      title='Make You Selection'
      isOpen={open}
    >
      <div className={Classes.DIALOG_BODY}>
        <MultiSelect
          createNewItemFromQuery={maybeCreateNewItemFromQuery}
          createNewItemRenderer={maybeCreateNewItemRenderer}
          initialContent={initialContent}
          itemRenderer={renderFilm}
          itemsEqual={areFilmsEqual}
          // we may customize the default filmSelectProps.items by
          // adding newly created items to the list, so pass our own
          items={filmsState.items}
          noResults={<MenuItem disabled={true} text='No results.' />}
          onItemSelect={handleFilmSelect}
          onItemsPaste={handleFilmsPaste}
          popoverProps={{ minimal: popoverMinimal }}
          tagRenderer={renderTag}
          tagInputProps={{
            onRemove: handleTagRemove,
            rightElement: clearButton,
            tagProps: getTagProps,
          }}
          selectedItems={filmsState.films}
        />
      </div>
      <div className={Classes.DIALOG_FOOTER}>
        <div className={Classes.DIALOG_FOOTER_ACTIONS}>
          <Tooltip content='Cancel'>
            <Button
              intent={Intent.DANGER}
              type='button'
              onClick={() => handleClose(false)}
            >
              Close
            </Button>
          </Tooltip>
          <Tooltip content='Save your Selection'>
            <Button
              intent={Intent.PRIMARY}
              type='button'
              onClick={() => handleClose(true)}
            >
              Save
            </Button>
          </Tooltip>
        </div>
      </div>
    </Dialog>
  )
}
export default SelectDialog
