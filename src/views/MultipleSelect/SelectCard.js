import React from 'react'
import { Button, Card, Elevation } from '@blueprintjs/core'
const SelectCard = (props) => {
  const { title, films, handleClick } = props
  return (
    <Card
      interactive
      elevation={Elevation.TWO}
      style={{ maxWidth: '500px', margin: 'auto' }}
    >
      <h5>{title || 'Film List'}</h5>
      <ul>
        {films?.length > 0 ? (
          films.map((film, index) => <li key={film.id}>{film.title}</li>)
        ) : (
          <p>No Films</p>
        )}
      </ul>
      <Button
        icon='multi-select'
        large
        type='button'
        text='Open Dialog'
        intent='primary'
        onClick={() => handleClick(true)}
      />
    </Card>
  )
}
export default SelectCard
