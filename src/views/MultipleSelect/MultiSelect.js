import React from 'react'
import { Button, MenuItem } from '@blueprintjs/core'
import { MultiSelect } from '@blueprintjs/select'

const MSelect = ({
  data,
  placeholder,
  onItemSelect,
  selectedItems,
  onDeselect,
  onClear,
  error,
  id,
  label,
  required
}) => {
  const itemRenderer = (item, { modifiers, handleClick }) => {
    return (
      <MenuItem
        active={modifiers.active}
        key={item.id}
        text={item.text}
        onClick={handleClick}
        shouldDismissPopover
      />
    )
  }

  const handleClick = (item) => {
    onItemSelect && onItemSelect(item)
  }

  const itemPredicate = (query, item) => {
    return (
      item.text && item.text.toLowerCase().indexOf(query.toLowerCase()) >= 0
    )
  }

  const handleTagRemove = (tag, index) => {
    onDeselect && onDeselect(tag, index)
  }

  const clearButton =
    selectedItems && selectedItems.length > 0 ? (
      <Button icon='trash' minimal onClick={() => onClear && onClear()} />
    ) : undefined

  return (
    <div>
      {label && (
        <label className='form-label' htmlFor={id || 'bpDiv'}>
          {label}
          {required && <span className='text-danger'>*</span>}
        </label>
      )}
      <div id={id || 'bpDiv'}>
        <MultiSelect
          items={data}
          placeholder={placeholder}
          fill
          itemRenderer={itemRenderer}
          itemPredicate={itemPredicate}
          onItemSelect={handleClick}
          tagRenderer={(item) => (item ? item.text : '')}
          tagInputProps={{
            rightElement: clearButton,
            onRemove: handleTagRemove
          }}
          selectedItems={selectedItems}
          popoverProps={{ minimal: true }}
        >
          <Button text={placeholder} rightIcon='caret-down' />
        </MultiSelect>
      </div>
      <div className='invalid-feedback' style={{ display: 'block' }}>
        {error}
      </div>
    </div>
  )
}

export default MSelect
