import React, { useState } from 'react'
import { Button, Classes, Dialog, Intent, Tooltip } from '@blueprintjs/core'
// import { MultiSelect } from "@blueprintjs/select"
import TOP_100_FILMS from './films'
import MSelect from './MultiSelect'

const SelectDialog = ({ closed, open }) => {
  const [films, setFilms] = useState(TOP_100_FILMS)
  const [selectedFilms, setSelectedFilms] = useState([])

  const selectFilm = (item) => {
    // film geliyor item ile
    const newSelected = [...selectedFilms, item]
    const newFilmList = films.filter((film) => film.id !== item.id)
    setFilms(newFilmList)
    setSelectedFilms(newSelected)
  }
  const handleDeselectFilm = (val) => {
    const delItem = TOP_100_FILMS.filter((film) => film.title === val)[0]
    const newFilmList = [...films, delItem]
    const newSelected = selectedFilms.filter((sFilm) => sFilm.id !== delItem.id)
    setFilms(newFilmList)
    setSelectedFilms(newSelected)
  }
  const handleClearSelection = () => {
    setFilms(TOP_100_FILMS)
    setSelectedFilms([])
  }
  const handleClose = (data) => {
    closed({ accept: data, selectedFilms })
  }
  return (
    <Dialog
      icon='info-sign'
      onClose={() => handleClose(false)}
      title='Make You Selection'
      isOpen={open}
    >
      <div className={Classes.DIALOG_BODY}>
        <MSelect
          placeholder='Multi Select'
          data={films}
          selectedItems={selectedFilms || []}
          onItemSelect={(item) => {
            selectFilm(item)
          }}
          onDeselect={handleDeselectFilm}
          onClear={handleClearSelection}
          label='Make Select'
          required
        />
      </div>
      <div className={Classes.DIALOG_FOOTER}>
        <div className={Classes.DIALOG_FOOTER_ACTIONS}>
          <Tooltip content='Cancel'>
            <Button
              intent={Intent.DANGER}
              type='button'
              onClick={() => handleClose(false)}
            >
              Close
            </Button>
          </Tooltip>
          <Tooltip content='Save your Selection'>
            <Button
              intent={Intent.PRIMARY}
              type='button'
              onClick={() => handleClose(true)}
            >
              Save
            </Button>
          </Tooltip>
        </div>
      </div>
    </Dialog>
  )
}
export default SelectDialog
