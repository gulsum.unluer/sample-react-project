import React from "react"
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom"
import "./global.scss"
// import 'normalize.css'
// import '@blueprintjs/core/lib/css/blueprint.css'
// import '@blueprintjs/icons/lib/css/blueprint-icons.css'

import Home from "./views/Home"
import About from "./views/About"
import AboutDetail from "./views/AboutDetail"
import MultipleSelect from "./views/MultipleSelect"
function App() {
  return (
    <Router>
      <div>
        <nav>
          <ul>
            <li>
              <Link to='/'>Home</Link>
            </li>
            <li>
              <Link to='/about'>About</Link>
            </li>
            <li>
              <Link to='/ms'>Multiple Select</Link>
            </li>
          </ul>
        </nav>

        {/* A <Switch> looks through its children <Route>s and
          renders the first one that matches the current URL. */}
        <Switch>
          <Route path='/ms'>
            <MultipleSelect />
          </Route>
          <Route path='/about'>
            <About />
          </Route>
          <Route path='/'>
            <Home />
          </Route>
        </Switch>
      </div>
    </Router>
  )
}

export default App
