import React from 'react'
import { Button, Dialog, Classes, Tooltip } from '@blueprintjs/core'

export default function BPModal({ show, title, onClose, onAccept, children }) {
  return (
    <div>
      <Dialog icon='info-sign' onClose={onClose} title={title} isOpen={show}>
        <div className={Classes.DIALOG_BODY}>{children}</div>
        <div className={Classes.DIALOG_FOOTER}>
          <div className={Classes.DIALOG_FOOTER_ACTIONS}>
            <Tooltip content='Close...'>
              <Button onClick={onClose}>Close</Button>
            </Tooltip>
            <Tooltip content='Okay...'>
              <Button onClick={onAccept}>Okay</Button>
            </Tooltip>
          </div>
        </div>
      </Dialog>
    </div>
  )
}
