import React from 'react'
import { InputGroup } from '@blueprintjs/core'

export default function BPInput ({ className, defaultValue, inputRef, placeholder, type, value, onChange }) {
  return (
    <InputGroup
      className={className}
      defaultValue={defaultValue}
      inputRef={inputRef}
      placeholder={placeholder}
      type={type}
      value={value}
      onChange={(e) => onChange(e.target.value)}
    />
  )
}
