import React from 'react'
import { Button, Intent } from '@blueprintjs/core'

import './style.css'

const BPButton = (props) => {
  const { type, className, text, icon, onClick, intent } = props

  const handleOnClick = () => {
    onClick && onClick()
  }

  return (
    <Button
      type={type}
      className={className}
      icon={icon}
      onClick={handleOnClick}
      intent={`${intent || Intent.SUCCESS}`}
    >
      {text}
    </Button>
  )
}

export default BPButton
