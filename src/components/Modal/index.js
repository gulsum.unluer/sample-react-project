import React, { useState, useImperativeHandle } from "react";
import { Dialog, Classes, AnchorButton, Intent } from "@blueprintjs/core";

const Modal = React.forwardRef(
  ({ title, onClose, onAccept, children }, ref) => {
    const [showModal, setShowModal] = useState(false);

    useImperativeHandle(ref, () => ({
      show() {
        setShowModal(true);
      },
      hide() {
        setShowModal(false);
      },
    }));

    return (
      <>
        {showModal && (
          <Dialog
            title={title}
            isOpen={showModal}
            onClose={onClose}
            onAccept={onAccept}
          >
            <div className={Classes.DIALOG_BODY}>{children}</div>
            <div>
              <div className={Classes.DIALOG_FOOTER}>
                <div className={Classes.DIALOG_FOOTER_ACTIONS}>
                  <AnchorButton onClick={onClose}>Vazgeç</AnchorButton>
                  <AnchorButton onClick={onAccept}>Tamam</AnchorButton>
                </div>
              </div>
            </div>
          </Dialog>
        )}
      </>
    );
  }
);

export default Modal;
