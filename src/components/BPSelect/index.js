import React from 'react'
import { Button, MenuItem } from '@blueprintjs/core'
import { Select } from '@blueprintjs/select'

export default function BPSelect ({ items, onItemSelect, value, placeholder }) {
  const itemRenderer = (item, { modifiers, handleClick }) => {
    return (
      <MenuItem
        active={modifiers.active}
        key={item.id}
        text={item.text}
        onClick={handleClick}
        shouldDismissPopover
      />
    )
  }
  const itemPredicate = (query, item) => {
    return (
      item.text &&
      item.text
        .toLocaleUpperCase('tr')
        .indexOf(query.toLocaleUpperCase('tr')) >= 0
    )
  }

  const handleClick = (item) => {
    onItemSelect && onItemSelect(item)
  }

  const truncateText = (value) => {
    return value && value.text ? value.text : placeholder
  }

  return (
    <div>
      <Select
        items={items}
        itemPredicate={itemPredicate}
        itemRenderer={itemRenderer}
        onItemSelect={handleClick}
        noResults={<MenuItem disabled text='No results.' />}
      >
        {/* children become the popover target; render value here */}
        <Button text={truncateText(value)} rightIcon='double-caret-vertical' />
      </Select>
    </div>
  )
}
